const mongoose = require("mongoose");

/**Instrução errada 
const { Schema } = require("monoose");
*/

//Instrução Correta
//const Schema = mongoose.Schema;


/**Instrução Errada 
const schema = new Schema({
  name,
  cpf,
  email,
});
*/

//Instrução Correta
const schema = new mongoose.Schema({
  name: String,
  cpf: String,
  email: String,
  phone: String,
  uf: String,
  subject: String
});


const model = mongoose.model("conversation", schema);

/**Instrução errada */
//module.exports = { model };

/**Instrução correta */
module.exports = model;