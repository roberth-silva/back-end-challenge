const conversation = require('../services/conversation');

class ConversationController{    
    
    async index(req, res) {
        const data = await conversation.find({});
        return res.json(data);
        console.log(data);
    }

    async store(req, res) {             
        const data = await conversation.create(req.body);    
        return res.json(data);
    }


    async show(req, res){
        const data = await conversation.findById(req.params.id);
        return res.json(data);
    }

    async update(req, res){
        const data = await conversation.findByIdAndUpdate(req.params.id, req.body, {new : true});
        return res.json(data);
    }

    async destroy(req, res){
        const data = await conversation.findOneAndRemove(req.params.id);
        return res.send();
    }

}

module.exports = new ConversationController();