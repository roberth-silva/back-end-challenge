const express = require("express");
const cors = require('cors');
const mongoose = require("mongoose");

const routes = require('./routes');

const db = require('./database/connection');

const app = express();

app.use(cors());
app.use(express.json());
app.use(routes);

mongoose.connect(db.uri, { useNewUrlParser: true });

/** 
 * app.route("/insert").all((req, res, next) => {
  model.create(req.body);
});*/

const port = 3001;

app.listen(port, () => {
  console.log("\n Server is running on port 3001.");
});